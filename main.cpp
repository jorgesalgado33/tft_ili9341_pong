/**
 * @file main.cpp
 * @author Jorge Salgado (jorgesalgado23@gmail.com)
 * @brief GAME PONG with ESP32 and TFT ILI9341
 * @version 
 * @date 01-01-2021
 * 
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDES - ARDUINO
 *******************************************************************************************************************************************/
#include <Arduino.h>
#include "../lib//Definition/GlobalDef.h"
#include <Adafruit_STMPE610.h>
/*******************************************************************************************************************************************
 *  												INCLUDES - 
 *******************************************************************************************************************************************/

// This optional setting causes Encoder to use more optimized code,
// It must be defined before Encoder.h is included.
#include "SPI.h"
#include "Wire.h"
#include "Adafruit_ILI9341.h"
#include "TouchScreen.h"
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/

#define SIZE_FILTER 6
#define SIZE_PLAYER 50
#define SIZE_JUMP 10
#define RATIO_BALL 5
#define BUTTON_UP 13
#define BUTTON_DONW 23

/*Touch screem PINs*/
#define TS_IRQ 33
#define TS_DO 25
#define TS_DI 26
#define TS_CS 27
#define TS_CLK 14


/*Screem PINs*/
#define SC_CS 15
#define SC_DC 4
#define SC_MOSI 5
#define SC_SCLK 18
#define SC_RST 2
#define SC_MISO 21
#define LED_BUILTIN 19


#define UP_PLAYER 1
#define DOWN_PLAYER 0

unsigned long encoder2lastToggled;
bool encoder2Paused = false;
Adafruit_ILI9341 *screem_tft;
Adafruit_STMPE610 *touchSc;
// Task declaration
TaskHandle_t TaskCore0, TaskCore1;

// Timer declaration
hw_timer_t * timer0 = NULL;
hw_timer_t * timer1 = NULL;
hw_timer_t * timer2 = NULL;
hw_timer_t * timer3 = NULL;
portMUX_TYPE timerMux0 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux1 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux2 = portMUX_INITIALIZER_UNLOCKED;
portMUX_TYPE timerMux3 = portMUX_INITIALIZER_UNLOCKED;

// Timer Flags
bool flagTimer0 = false;
bool flagTimer1 = false;
bool flagTimer2 = false;
bool flagTimer3 = false;

uint8_t player1 = 2;
uint8_t player2 = 2;
uint8_t player1Points = 0;
uint8_t player2Points = 0;

uint8_t temp = UP_PLAYER;
uint8_t temp2 = UP_PLAYER;
/*******************************************************************************************************************************************
 *  												TEST
 *******************************************************************************************************************************************/
uint32_t test_counter = 0;

enum Direction{
    Dir_UP,
    Dir_DOWN,
    Dir_LEFT,
    Dir_RIGHT,
};

struct Ball{
    uint8_t speed_ball;
    uint16_t Pos_J_ball;
    uint16_t Pos_I_ball;
    uint16_t Pos_J_ball_Prev;
    uint16_t Pos_I_ball_Prev;
    Direction HorizDirection;
    Direction VertDirection;
};

Ball BallPong;

void GameInit();
void DisplayTable();
void ActionButtons();
void CheckDirectionV();
void CheckDirectionH();


/*******************************************************************************************************************************************
 *  												CORE LOOPS
 *******************************************************************************************************************************************/
// Loop of core 0
void LoopCore0( void * parameter ){
    while(true) {

        /*if (touchSc->bufferEmpty()) {
            return;
        }
        delay(1000);
        TS_Point p = touchSc->getPoint();
        Serial.print("X = "); Serial.print(p.x);
        Serial.print("\tY = "); Serial.print(p.y);
        Serial.print("\tPressure = "); Serial.println(p.z); */
        // Code for Timer 0 interruption
        if (flagTimer0){
            flagTimer0 = false;      

        }

        // Code for Timer 1 interruption
        if (flagTimer1){
            flagTimer1 = false;
        }



        // Delay to feed WDT
        delay(10);
    }
}

// Loop of core 1
void LoopCore1( void * parameter ){



    while(true) {
 
        CheckDirectionV();
        CheckDirectionH();

        screem_tft->setCursor(50,10);
        screem_tft->printf("Player1 : %d",player1Points);
        screem_tft->setCursor((ILI9341_TFTHEIGHT/2)+50,10);
        screem_tft->printf("Player2 : %d",player2Points);

        /*new position ball*/
        screem_tft->drawCircle(BallPong.Pos_I_ball,BallPong.Pos_J_ball,RATIO_BALL,ILI9341_WHITE);
        /*erase tail ball*/
        screem_tft->drawCircle(BallPong.Pos_I_ball_Prev,BallPong.Pos_J_ball_Prev,RATIO_BALL,ILI9341_BLACK);
        /*new position ball*/
        screem_tft->drawCircle(BallPong.Pos_I_ball,BallPong.Pos_J_ball,RATIO_BALL,ILI9341_WHITE);

        screem_tft->setCursor((ILI9341_TFTHEIGHT/2)-30,ILI9341_TFTWIDTH-10);
        screem_tft->print("Jorge Salgado");

        BallPong.Pos_J_ball_Prev = BallPong.Pos_J_ball;
        BallPong.Pos_I_ball_Prev = BallPong.Pos_I_ball;
        delay(1);

        ActionButtons();

   
        // Code for Timer 2 interruption
        if (flagTimer2){
            flagTimer2 = false;

            // ========== Code ==========

            // ==========================
        }

        // Code for Timer 3 interruption
        if (flagTimer3){
            flagTimer3 = false;

            // ========== Code ==========

            // ==========================
        }

        // Delay to feed WDT
        delay(1);
    }
}

/*******************************************************************************************************************************************
 *  												TIMERS
 *******************************************************************************************************************************************/
// TIMER 0
void IRAM_ATTR onTimer0(){
    portENTER_CRITICAL_ISR(&timerMux0);

    flagTimer0 = true;

    portEXIT_CRITICAL_ISR(&timerMux0);
}

// TIMER 1
void IRAM_ATTR onTimer1(){
    portENTER_CRITICAL_ISR(&timerMux1);

    flagTimer1 = true;

    portEXIT_CRITICAL_ISR(&timerMux1);
}

// TIMER 2
void IRAM_ATTR onTimer2(){
    portENTER_CRITICAL_ISR(&timerMux2);

    flagTimer2 = true;

    portEXIT_CRITICAL_ISR(&timerMux2);
}

// TIMER 3
void IRAM_ATTR onTimer3(){
    portENTER_CRITICAL_ISR(&timerMux3);

    flagTimer3 = true;

    portEXIT_CRITICAL_ISR(&timerMux3);
}

/*******************************************************************************************************************************************
 *  												SETUP
 *******************************************************************************************************************************************/
void setup() {

    // Disable brownout detector
    //WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0); 
    //rtc_wdt_protect_off();
    //rtc_wdt_disable();

    // Serial Port
    Serial.begin(115200);
    Serial.println(" +++++ ESP32 TESTING +++++");
    
    /*************************************************************************************************/
    /*************************************************************************************************/
    screem_tft = new Adafruit_ILI9341(SC_CS,SC_DC,SC_MOSI,SC_SCLK,SC_RST,SC_MISO);
    screem_tft->begin();

    pinMode(LED_BUILTIN, OUTPUT);    
    pinMode(BUTTON_UP,INPUT_PULLUP);
    pinMode(22,OUTPUT);
    pinMode(BUTTON_DONW,INPUT_PULLUP);
    
    
    digitalWrite(LED_BUILTIN,HIGH);
    digitalWrite(22,LOW);


    BallPong.speed_ball = 1;
    GameInit();
    /*****************************************************************************************/
 
    /*****************************************************************************************/
    

    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    /************************************************************************************************/
   
    // Task of core 0
    xTaskCreatePinnedToCore(
        LoopCore0,  /* Function to implement the task */
        "LoopCore0",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore0, /* Task handle. */
        0);         /* Core where the task should run */

    delay(500);  // needed to start-up task1

    // Task of core 1
    xTaskCreatePinnedToCore(
        LoopCore1,  /* Function to implement the task */
        "LoopCore1",    /* Name of the task */
        10000,      /* Stack size in words */
        NULL,       /* Task input parameter */
        1,          /* Priority of the task */
        &TaskCore1, /* Task handle. */
        1);         /* Core where the task should run */

    // Timer0
    Serial.println("start timer 0");
    timer0 = timerBegin(0, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer0, &onTimer0, true); // edge (not level) triggered 
    timerAlarmWrite(timer0, 5000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer1
    Serial.println("start timer 1");
    timer1 = timerBegin(1, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer1, &onTimer1, true); // edge (not level) triggered 
    timerAlarmWrite(timer1, 2000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer2
    Serial.println("start timer 2");
    timer2 = timerBegin(2, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer2, &onTimer2, true); // edge (not level) triggered 
    timerAlarmWrite(timer2, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Timer3
    Serial.println("start timer 3");
    timer3 = timerBegin(3, 80, true);  // timer 0, MWDT clock period = 12.5 ns * TIMGn_Tx_WDT_CLK_PRESCALE -> 12.5 ns * 80 -> 1000 ns = 1 us, countUp
    timerAttachInterrupt(timer3, &onTimer3, true); // edge (not level) triggered 
    timerAlarmWrite(timer3, 1000000, true); // 1000000 * 1 us = 1 s, autoreload true

    // Enable the timer alarms
    timerAlarmEnable(timer0); // enable
    //timerAlarmEnable(timer1); // enable
    //timerAlarmEnable(timer2); // enable
    //timerAlarmEnable(timer3); // enable
}

/*******************************************************************************************************************************************
 *  												Main Loop
 *******************************************************************************************************************************************/
// Loop not used
void loop() {
    vTaskDelete(NULL);
}

void GameInit(){

    player1 =(ILI9341_TFTWIDTH/2)-(SIZE_PLAYER/2);
    player2 =(ILI9341_TFTWIDTH/2)-(SIZE_PLAYER/2);

    screem_tft->setRotation(1);
    screem_tft->fillScreen(ILI9341_BLACK);
    screem_tft->drawFastVLine(1,player1,SIZE_PLAYER,ILI9341_WHITE);   
    screem_tft->drawFastVLine(2,player1,SIZE_PLAYER,ILI9341_WHITE);
    screem_tft->drawFastVLine(3,player1,SIZE_PLAYER,ILI9341_WHITE);
    screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2,SIZE_PLAYER,ILI9341_WHITE); 
    screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2,SIZE_PLAYER,ILI9341_WHITE); 
    screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2,SIZE_PLAYER,ILI9341_WHITE); 

       /*Ball init*/
    BallPong.Pos_J_ball = ILI9341_TFTWIDTH/2;
    BallPong.Pos_I_ball = ILI9341_TFTHEIGHT/2;

    screem_tft->drawCircle(BallPong.Pos_I_ball,BallPong.Pos_J_ball,RATIO_BALL,ILI9341_WHITE);

    BallPong.Pos_I_ball_Prev = BallPong.Pos_I_ball;
    BallPong.Pos_J_ball_Prev = BallPong.Pos_J_ball;

    BallPong.VertDirection = Dir_UP;
    BallPong.HorizDirection = Dir_LEFT;    

    screem_tft->setCursor((ILI9341_TFTHEIGHT/2)-30,ILI9341_TFTWIDTH-10);
    screem_tft->print("Jorge Salgado");
    BallPong.speed_ball=1;
}

void ActionButtons(){
    if(HIGH == digitalRead(BUTTON_UP))
    {
        delay(5);
        //Serial.printf("a"); 
    }
    else{//LOW
        
        delay(50);
        if(UP_PLAYER == temp)
        {
            if(player1 > SIZE_JUMP){
                player1-=SIZE_JUMP;
                /*New position*/
                screem_tft->drawFastVLine(1,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(2,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(3,player1,SIZE_PLAYER,ILI9341_WHITE);
                /*erase the tail */
                screem_tft->drawFastVLine(1,player1+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(2,player1+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(3,player1+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                    Serial.printf("a");                
            }
            else{
                temp = DOWN_PLAYER;
            }
        }
        else{
            if(player1 < ILI9341_TFTWIDTH-SIZE_PLAYER){
                player1 +=SIZE_JUMP;   
                /*New position*/            
                screem_tft->drawFastVLine(1,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(2,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(3,player1,SIZE_PLAYER,ILI9341_WHITE);
                /*erase teh tail*/
                screem_tft->drawFastVLine(1,player1-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(2,player1-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(3,player1-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                Serial.printf("w");
            }
            else{
                temp = UP_PLAYER;
            }
        }

            

    }


    if(HIGH==digitalRead(BUTTON_DONW))
    {
        delay(5);
    }else{
        delay(50);
        if(UP_PLAYER == temp2)
        {
            if(player2 > SIZE_JUMP){
            player2-=SIZE_JUMP;
            /*New position*/
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2,SIZE_PLAYER,ILI9341_WHITE);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2,SIZE_PLAYER,ILI9341_WHITE);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2,SIZE_PLAYER,ILI9341_WHITE);
            /*erase the tail */
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
            Serial.printf("9");
            }
            else{
            temp2 = DOWN_PLAYER;
            }
        }
        else
        {
            if(player2 < ILI9341_TFTWIDTH-SIZE_PLAYER){
            player2 +=SIZE_JUMP;   
            /*New position*/            
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2,SIZE_PLAYER,ILI9341_WHITE);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2,SIZE_PLAYER,ILI9341_WHITE);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2,SIZE_PLAYER,ILI9341_WHITE);
            /*erase teh tail*/
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
            screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
            Serial.printf("3");
            }
            else{
                temp2 = UP_PLAYER;
            }
        }
       
    }
    
        
     if(Serial.available()){
        char keyword = Serial.read();
        switch(keyword){
            case 'A':
            case 'a':
            if(player1 > SIZE_JUMP){
                player1-=SIZE_JUMP;
                /*New position*/
                screem_tft->drawFastVLine(1,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(2,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(3,player1,SIZE_PLAYER,ILI9341_WHITE);
                /*erase the tail */
                screem_tft->drawFastVLine(1,player1+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(2,player1+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(3,player1+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                Serial.printf("a");
                
            }
            break;
            case 'W':
            case 'w':
            if(player1 < ILI9341_TFTWIDTH-SIZE_PLAYER){
                player1 +=SIZE_JUMP;   
                /*New position*/            
                screem_tft->drawFastVLine(1,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(2,player1,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(3,player1,SIZE_PLAYER,ILI9341_WHITE);
                /*erase teh tail*/
                screem_tft->drawFastVLine(1,player1-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(2,player1-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(3,player1-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                Serial.printf("w");
            }
            break;
            case '9':
            if(player2 > SIZE_JUMP){
                player2-=SIZE_JUMP;
                /*New position*/
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2,SIZE_PLAYER,ILI9341_WHITE);
                /*erase the tail */
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2+SIZE_PLAYER,SIZE_JUMP,ILI9341_BLACK);
                Serial.printf("9");
            }
            break;
            case '3':
            if(player2 < ILI9341_TFTWIDTH-SIZE_PLAYER){
                player2 +=SIZE_JUMP;   
                /*New position*/            
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2,SIZE_PLAYER,ILI9341_WHITE);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2,SIZE_PLAYER,ILI9341_WHITE);
                /*erase teh tail*/
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-1,player2-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-2,player2-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                screem_tft->drawFastVLine(ILI9341_TFTHEIGHT-3,player2-SIZE_JUMP,SIZE_JUMP,ILI9341_BLACK);
                Serial.printf("3");
            }
            break;
            case 'p':
            BallPong.speed_ball+=1;

            break;
            case 'm':
            BallPong.speed_ball-=1;
            
            break;

        }
    }
    

}


void CheckDirectionV(){
    if( Dir_UP == BallPong.VertDirection){

        if(BallPong.Pos_J_ball > RATIO_BALL+BallPong.speed_ball){
            BallPong.Pos_J_ball -=BallPong.speed_ball;
        }
        else{
            BallPong.VertDirection = Dir_DOWN;
        }

    }
    else{//DOWN
        if(BallPong.Pos_J_ball < (ILI9341_TFTWIDTH-(RATIO_BALL)-BallPong.speed_ball))
        {
            BallPong.Pos_J_ball +=BallPong.speed_ball;
        }
        else{
            BallPong.VertDirection = Dir_UP;
        }
    }

}
void CheckDirectionH(){

    if(Dir_LEFT == BallPong.HorizDirection){

        if(BallPong.Pos_I_ball > RATIO_BALL+4+BallPong.speed_ball){
            BallPong.Pos_I_ball -=BallPong.speed_ball;
        }
        else{

            if(BallPong.Pos_I_ball < RATIO_BALL+5+BallPong.speed_ball){

                if((BallPong.Pos_J_ball <= player1+(SIZE_PLAYER))&&(BallPong.Pos_J_ball >= player1)){
                    Serial.printf("%d",BallPong.Pos_J_ball);
                    BallPong.speed_ball+=1;
                }
                else{
                    player2Points +=1;
                    GameInit();
                }

            }


            BallPong.HorizDirection = Dir_RIGHT;
        }
        
    }
    else{//Right
        if(BallPong.Pos_I_ball <(ILI9341_TFTHEIGHT-9-BallPong.speed_ball)){
            BallPong.Pos_I_ball +=BallPong.speed_ball;
        }
        else{

            if(BallPong.Pos_I_ball > ILI9341_TFTHEIGHT-10-BallPong.speed_ball){
                if((BallPong.Pos_J_ball <= player2+(SIZE_PLAYER))&&(BallPong.Pos_J_ball >= player2)){
                    BallPong.speed_ball+=1;
                }
                else{
                    player1Points +=1;
                    GameInit();
                }

            }
            BallPong.HorizDirection = Dir_LEFT;
        }
        
    }

}

